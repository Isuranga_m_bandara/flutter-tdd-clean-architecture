import 'package:dartz/dartz.dart';
import 'package:flutter_application_1/core/util/input_converter.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  InputConverter inputConverter;

  setUp(() {
    inputConverter = InputConverter();
  });

  group('stringToUnsinedInt', () {
    test(
        'should return an interger when the string represent an unsigned integer',
        () async {
      final str = '123';

      final result = inputConverter.stringToUnsignedInteger(str);

      expect(result, Right(123));
    });

    test(
      'should return a Failure when the string is not an integer',
       () async {

      final str = 'abc';

      final result = inputConverter.stringToUnsignedInteger(str);

      expect(result, Left(InvalidInputFailure()));
      
    });

    test(
      'should return a Failure when the string is a negative integer',
       () async {

      final str = '-123';

      final result = inputConverter.stringToUnsignedInteger(str);

      expect(result, Left(InvalidInputFailure()));
      
    });
  });
}
